<div align="center">
<h1 align="center">
  <img src="https://assets.gitlab-static.net/uploads/-/system/project/avatar/18762291/layanlogo.png" alt="Project">
  <br />
  Layan Themes for Snap
</h1>
</div>

<p align="center"><b>These are the Layan GTK themes for snaps.</b> Layan is a Material Design theme for GNOME/GTK based desktop environments.

This theme is based on the <a href="https://github.com/nana-4/materia-theme">Materia theme</a> of nana-4. 


<b>Attributions:</b>
This snap is packaged from vinceliuice's <a href="https://github.com/vinceliuice/Layan-gtk-theme">Layan-gtk-theme</a>. The Layan theme is under a GPL3.0 licence.
</p>


## Install
<a href="https://snapcraft.io/layan-themes">
<img alt="Get it from the Snap Store" src="https://snapcraft.io/static/images/badges/en/snap-store-white.svg" />
</a>

You can install the snap from the Snap Store оr by running:
```
sudo snap install layan-themes
```
To connect the theme to an app run:
```
sudo snap connect [other snap]:gtk-3-themes layan-themes:gtk-3-themes 
```

## Icons
If you are using the Tela icon theme along with the Layan theme, you need to install ```tela-icons``` from the Snap Store in order to see the app icons in the store properly. Please run:

```
sudo snap install tela-icons
```

